import re
import csv
import os 


class ar_mobile_parce(object):
    default_indicativo = ''
    codigo_indicativo = {'9120': ['280'], '9200': ['2945'], '9203': ['2945'], '8153': ['291'], '8307': ['299'], '8305': ['299'], '8303': ['299'], '8301': ['299'], '8300': ['299'], '8347': ['2942'], '8309': ['299'], '0': [], '9400': ['2902', '2966'], '8105': ['291'], '8148': ['291'], '3600': ['370'], '8101': ['291'], '8103': ['299'], '8142': ['291'], '8109': ['2932'], '8146': ['291'], '3400': ['379'], '8336': ['298'], '9100': ['280'], '9020': ['297'], '9011': ['297'], '8361': ['298'], '8360': ['298'], '8363': ['298'], '8364': ['298'], '9107': ['280'], '9105': ['280'], '9103': ['280'], '3630': ['3715'], '9015': ['297'], '9017': ['297'], '8328': ['298'], '8324': ['299'], '8136': ['291'], '8326': ['299'], '8132': ['291'], '9220': ['2945'], '8520': ['2920'], '8370': ['2920'], '9420': ['2964'], '7530': ['291'], '8160': ['291'], '9111': ['297'], '8315': ['2942'], '8316': ['299'], '9001': ['297'], '9000': ['297'], '8000': ['291'], '9410': ['2901'], '7540': ['2926']}
    dict_indicativos = {}
    known_structures = [
        ('(\D|^)(0)*([0-9][0-9][0-9])*(\-)*(15)*(\-)*([3-6])(\-)*([0-9][0-9][0-9][0-9][0-9][0-9])(\D|$)',
            {
                'indicativo':2,
                'numero':[6,8]
            }
        ),
        ('(\D|^)(0)*([0-9][0-9][0-9][0-9])*(\-)*(15)*(\-)*([2-6])(\-)*([0-9][0-9][0-9][0-9][0-9])(\D|$)',
            {
                'indicativo':2,
                'numero':[6,8]
            }
        ),
        ('(\D|^)(0)*([0-9][0-9])*(\-)*(15)*(\-)*([3-6])(\-)*([0-9][0-9][0-9][0-9][0-9][0-9][0-9])(\D|$)',
            {
                'indicativo':2,
                'numero':[6,8]
            }
        ),
        ('(^|[!0-9])(15)*(\-)*([4|5|6])(\-)*([0-9][0-9][0-9][0-9][0-9][0-9])(\D|$)',
            {
                'indicativo':False,
                'numero':[3,4]
            }
        ),
        ('(^|[!0-9])(15)*(\-)*([4|5|6])(\-)*([0-9][0-9][0-9][0-9][0-9])(\D|$)',
            {
                'indicativo':False,
                'numero':[3,4]
            }
        ),
        ('([!0-9|^])(15)([3-6])([0-9][0-9][0-9][0-9][0-9][0-9])([!0-9|$])',
            {
                'indicativo':False,
                'numero':[2,3]
            }
        ),
        ('([!0-9|^])(15)([3-6])([0-9][0-9][0-9][0-9][0-9])([!0-9|$])',
            {
                'indicativo':False,
                'numero':[2,3]
            }
        )
    ]
 
    def __init__(self):
        path = os.path.dirname(os.path.realpath(__file__))
        with open(path + '/data/enacom.csv', 'r') as csvfile:
         spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
         for row in spamreader:
            if row[1] !='SBT':
                self.add_dict_indicativos(row[4],row[5])


    def set_default_indicativo(self,indicativo):
        self.default_indicativo = indicativo

    def set_codigo_indicativo(self,code,indicativo):
        self.codigo_indicativo[code] = indicativo

    def get_codigo_indicativo(self,code):
        if code and code in self.codigo_indicativo :
            return self.codigo_indicativo[code]
        else :
            return [self.default_indicativo]

    def get_indicativo_bloques_size(self,indicativo):
        if indicativo in self.dict_indicativos :
            size = [len(x) for x in self.dict_indicativos[indicativo]]
            return list(set(size))
        else :
            return [0]

    def add_dict_indicativos(self,indicativo,bloque):
        if indicativo in self.dict_indicativos :
            self.dict_indicativos[indicativo].append(bloque)
        else :
            self.dict_indicativos[indicativo]=[bloque]

    def is_mobile(self,indicativo,bloque):
        if indicativo in self.dict_indicativos and \
            bloque in self.dict_indicativos[indicativo]:
            return True
        return False
    def make_dict(self,structure,mob):
        if structure == False:
             return ''
        if type(structure) is int:
            return mob[structure]
        elif type(structure) is list:
            rtn =[mob[x] for x in structure]
            return ''.join(rtn)

    def parce_text(self,text,it=0):
        mob = re.compile(self.known_structures[it][0]).findall(text)
        if len(mob):    
            return [{'indicativo': self.make_dict(self.known_structures[it][1]['indicativo'],x),
                    'numero': self.make_dict(self.known_structures[it][1]['numero'],x)} for x in mob]

    def format_e164(self,indicativo, number,country='54',mobile='9'):
        return "+%s%s%s%s"%(country,mobile,indicativo,number)
    def get_phones(self,text,code=False):
        rtn = []
        it = 0 
        while it < len(self.known_structures):
        
            phones =  self.parce_text(text,it)
            if phones:
                for phone in phones:
                    if phone['indicativo']: 
                        indicativos= [phone['indicativo']]
                    else:     
                        indicativos = self.get_codigo_indicativo(code)

                    for indicativo in indicativos:
                        for  bloque_size in self.get_indicativo_bloques_size(indicativo):
                            bloque = phone['numero'][:bloque_size]
                            if (len(indicativo) + len(phone['numero'])) == 10 and self.is_mobile(indicativo,bloque):
                                rtn.append(self.format_e164(indicativo,phone['numero']))
                if len(rtn):
                    return rtn
            it += 1
